# Download base image
FROM php:5.6.35-fpm-alpine3.4

# Set up maintainer
LABEL maintainer="habil@arkadiacorp.com"

# Environments
ENV TIMEZONE            Asia/Jakarta
ENV PHP_MEMORY_LIMIT    512M
ENV MAX_UPLOAD          50M
ENV PHP_MAX_FILE_UPLOAD 200
ENV PHP_MAX_POST        100M

# Update repo & install packages
RUN apk update && apk add bash && apk add tzdata && \
	cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime && \
	echo "Asia/Jakarta" > /etc/timezone && \
	apk add --update \
		autoconf \
		openssl-dev \
		g++ \
		nano \
		make \
		re2c \
		openssh \
		php5 \
		php5-apcu \
		php5-bz2 \
		php5-cli \
		php5-cgi \
		php5-curl \
		php5-ctype \
		php5-dev \
		php5-gmp \
		php5-gd \
		php5-gettext \
		php5-iconv \
		php5-json \
		php5-mcrypt \
		php5-mysql \
		php5-mysqli \
		php5-openssl \
		php5-pear \
		php5-pgsql \
		php5-pdo \
		php5-pdo_pgsql \
		php5-pdo_mysql \
		php5-pdo_sqlite \
		php5-pdo_dblib \
		php5-phar \
		php5-phalcon \
		php5-soap \
		php5-sqlite3 \
		php5-xml \
		php5-zip && \
		# install php mongo & redis module
		pecl install mongodb redis && \
		docker-php-ext-enable mongodb redis && \
		# Let's compile php5-fileinfo & bcmath from source since it's not available in alpine repo
		wget http://id1.php.net/distributions/php-5.6.34.tar.gz && \
		tar xzvf php-5.6.34.tar.gz && \
		cd php-5.6.34/ext/fileinfo && \
		phpize && ./configure --enable-fileinfo && \
		make && make install && \
		cd .. && cd bcmath && \
		phpize && ./configure --enable-bcmath && \
		make && make install && \
		# Last step, cleaning up
		cd /var/www/html && \
		rm -rf php-* && \
		# Install composer
		curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && \
		apk del tzdata autoconf openssl-dev g++ make && rm -rf /var/cache/apk/*

# Set environments
RUN	sed -i "s|;*date.timezone =.*|date.timezone = ${TIMEZONE}|i" /etc/php5/php.ini && \
	sed -i "s|;*memory_limit =.*|memory_limit = ${PHP_MEMORY_LIMIT}|i" /etc/php5/php.ini && \
  sed -i "s|;*upload_max_filesize =.*|upload_max_filesize = ${MAX_UPLOAD}|i" /etc/php5/php.ini && \
  sed -i "s|;*max_file_uploads =.*|max_file_uploads = ${PHP_MAX_FILE_UPLOAD}|i" /etc/php5/php.ini && \
  sed -i "s|;*post_max_size =.*|post_max_size = ${PHP_MAX_POST}|i" /etc/php5/php.ini && \
  sed -i "s|;*cgi.fix_pathinfo=.*|cgi.fix_pathinfo= 1|i" /etc/php5/php.ini && \
  # Let's modified the phalcon module, in order to make it work
  ln -s /usr/lib/php5/modules/phalcon.so /usr/local/lib/php/extensions/no-debug-non-zts-20131226 && \
  echo "extension=phalcon.so" > /usr/local/etc/php/conf.d/docker-php-ext-phalcon.ini && \
  echo "extension=fileinfo.so" > /usr/local/etc/php/conf.d/docker-php-ext-fileinfo.ini && \
  echo "extension=bcmath.so" > /usr/local/etc/php/conf.d/docker-php-ext-bcmath.ini && \
  # install this tcp package to speed up composer installation
  composer global require hirak/prestissimo